
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import objects.Record;
import objects.User;

@WebServlet(name = "SignServlet", urlPatterns = {"/SignServlet"})
public class SignServlet extends HttpServlet {

    //need to change! todo
    List<User> users = new ArrayList<User>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect("signIn.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userIn = request.getParameter("username");
        String passIn = request.getParameter("password");
        String realName = request.getParameter("realname");
        String email = request.getParameter("email");
        String icon = request.getParameter("icon");
        String empty = "";

        if (userIn.equals(empty) || passIn.equals(empty) || 
                realName.equals(empty) || email.equals(empty)) {
            request.setAttribute("error", true);
            request.getRequestDispatcher("signIn.jsp").forward(request, response);
        } else {
            
            User newUser = new User(userIn, passIn, realName, email, icon);
            users.add(newUser);
            
            HttpSession session = request.getSession();
            response.sendRedirect("secured/MenuServ");
        }

    }
}
