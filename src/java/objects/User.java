package objects;

public class User {

    String user;
    String password;
    String name;
    String email;
    String icon;

    public User(String user, String password, String name, String email, String icon) {
        this.user = user;
        this.password = password;
        this.name = name;
        this.email = email;
        this.icon = icon;
    }

    public String getUser() {
        return this.user;
    }

    public String getPassword() {
        return this.password;
    }

    public String getName() {
        return this.name;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public String getIcon() {
        return this.icon;
    }
}
