
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import objects.User;

@WebServlet(name = "MyFormServlet", urlPatterns = {"/MyFormServlet"})
public class MyFormServlet extends HttpServlet {

    List<User> users = new ArrayList<User>();
    

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect("login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //need to cahnge
        String a = "admin";
        String b = "pas";
        String stam = "stam";
        User fi = new User(a, b, stam, stam, stam);
        users.add(fi);

        String userIn = request.getParameter("username");
        String passIn = request.getParameter("password");
        
        
        for (Iterator<User> it = users.iterator(); it.hasNext();) {
            User nextUser = it.next();
            
            if(nextUser.getUser().equals(userIn) &&
                    nextUser.getPassword().equals(passIn)){
                HttpSession session = request.getSession();
                response.sendRedirect("secured/MenuServ");
                return;
            }
        }
        

        //not found
        request.setAttribute("error", true);
        request.getRequestDispatcher("login.jsp").forward(request, response);
        

    }
}
