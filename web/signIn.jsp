<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="signStyle.css"/>
        <title>Sign In</title>
    </head>
    <body>
        <form action="SignServlet" method="post">
            <%
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                out.println("<div>Current time=" + sdf.format(cal.getTime()) + "</div>");
            %>
            <div><%= (new Double(8))%></div>

            <div>Username:</div>
            <input type="text" name="username"/>

            <div>Password:</div>
            <input type="password" name="password"/>

            <div>Real Name:</div>
            <input type="text" name="realname"/>

            <div>E-mail:</div>
            <input type="text" name="email"/>

            <div>Icon:</div>
            <input type="text" name="email"/>

            <input type="submit"/>
            <input type="reset"/>
            <% if (request.getAttribute("error") != null
                        && (Boolean) request.getAttribute("error")) {%>
            <div>Wrong Input. Please try again</div>
            <% }%>

        </form>
    </body>
</html>